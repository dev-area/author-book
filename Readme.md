ТЗ:

Используя PHP 7 и фреймворк Symfony 5 (последние версии PHP 7.4 и Symfony 5.2), а также Doctrine ORM и с использованием Docker контейнера, написать REST API для создания и получения книг и авторов из базы данных в формате JSON.

Требования к заданию:
1. Написать миграцию, засеивающую тестовые таблицы ~10 000 книгами и ~10 000 авторами
2. Реализовать запросы на создание книги и автора в базе /book/create, /author/create
3. Реализовать запрос на получение списка книг с автором из базы /book/search c поиском по названию книги
4. Написать Unit-тест
5. Используя возможности Symfony по локализации контента, сделать мультиязычный метод получения информации о книге /{lang}/book/{Id}, где {lang} = en|ru и {Id} = Id книги. Формат ответа: {Id: 1, 'Name':'War and Peace|Война и мир'} - поле Name выводить на языке локализации запроса.

Пример формата сущностей:

Автор: {
    'Id': 1,
    'Name': 'Лев Толстой'
}

Книга: {
    'Id': 1,
    'Name': ' War and peace|Война и мир',
    'Author': [ {
        'Id': 1,
        'Name': 'Лев Толстой'
    } ]
}


ПО: Git, Docker, Composer, Postman, terminal

Клонировать проект:

git clone git@gitlab.com:dev-area/author-book.git

В терминале перейти в папку проекта:

composer install

docker-compose up -d


Запустить миграции
docker-compose exec php-fpm php bin/console doctrine:migrations:migrate -n

Засеять данными (10000 авторов и книг)
docker-compose exec php-fpm php bin/console doctrine:fixtures:load -n

В Postman, создать автора (/author/create):

http://localhost:8080/api/v1/author (POST)
Вкладка: Headers
установить key: Content-Type
установить value: application/json
Вкладка: Body
raw/JSON
{
"name": "Л. Толстой"
}

В Postman, создать книгу с автором (/book/create):

http://localhost:8080/api/v1/book   (POST)
Вкладка: Headers
установить key: Content-Type
установить value: application/json
Вкладка: Body
raw/JSON
{
"name": "War and Peace|Война и мир",
"author": [
            1
          ]
}

В Postman найти книгу (например: Мир)

http://localhost:8080/api/v1/book/search/Мир

В Postman мультиязычный метод (/{lang}/book/{Id})

http://localhost:8080/api/v1/en/book/1
или
http://localhost:8080/api/v1/ru/book/1

Тесты в терманале запустить:

docker-compose exec php-fpm vendor/bin/phpunit