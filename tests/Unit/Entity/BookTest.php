<?php


namespace App\Tests\Unit\Entity;


use App\Entity\Book;
use PHPUnit\Framework\TestCase;

class BookTest extends TestCase
{

    public function testGetName(): void
    {
        $value = 'War and Peace|Война и мир';
        $book = new Book();
        $book->setName($value);
        self::assertEquals($value,$book->getName());
        self::assertInstanceOf(Book::class, $book);
    }

}