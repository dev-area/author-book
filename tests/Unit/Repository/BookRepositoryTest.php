<?php


namespace App\Tests\Unit\Repository;


use App\Entity\Book;
use App\Repository\BookRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BookRepositoryTest extends KernelTestCase
{
    public function testFindAllByPartName()
    {
        self::bootKernel();

        $value = 'Мир';
        $books = self::$container->get(BookRepository::class)->findAllByPartName($value);

        $this->assertIsArray($books);
        $this->assertContainsOnlyInstancesOf(Book::class, $books);
    }

    public function testFindOneById()
    {
        self::bootKernel();
        $value = 1;
        $book = self::$container->get(BookRepository::class)->findOneById($value);

        $this->assertInstanceOf(Book::class, $book);
    }

}