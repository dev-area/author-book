<?php


namespace App\Tests\Unit\Service;


use App\Service\Book\BookService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BookServiceTest extends KernelTestCase
{

    public function testFindById(){
        self::bootKernel();

        $value = 'War';
        $books = self::$container->get(BookService::class)->searchBook($value);

        $this->assertIsArray($books);
    }
}