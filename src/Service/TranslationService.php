<?php


namespace App\Service;


use App\Entity\Book;
use Symfony\Component\HttpFoundation\Request;

class TranslationService
{

    /**
     * @param Book $book
     * @param Request $request
     * @return array
     */
    public function translate(Book $book, Request $request): array
    {
        $translations = explode("|", $book->getName());

        $response = [];
        $response['id'] = $book->getId();
        if ($request->getLocale() == 'en') {
            $response['name'] = $translations[0];
        } else {
            $response['name'] = $translations[1];
        }

        return $response;
    }
}