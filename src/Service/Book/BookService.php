<?php


namespace App\Service\Book;


use App\Entity\Book;
use App\Repository\BookRepositoryInterface;
use App\Service\TranslationService;
use Symfony\Component\HttpFoundation\Request;

class BookService
{
    /**
     * @var BookRepositoryInterface
     */
    private BookRepositoryInterface $bookRepository;
    /**
     * @var TranslationService
     */
    private TranslationService $translationService;

    /**
     * @param BookRepositoryInterface $bookRepository
     * @param TranslationService $translationService
     */
    public function __construct(BookRepositoryInterface $bookRepository,
                                TranslationService $translationService)
    {
        $this->bookRepository = $bookRepository;
        $this->translationService = $translationService;
    }

    /**
     * @param Book $book
     */
    public function saveBook(Book $book): void
    {
        $this->bookRepository->save($book);
    }

    /**
     * @param string $target
     * @return array
     */
    public function searchBook(string $target): array
    {
        return $this->bookRepository->findAllByPartName($target);
    }

    /**
     * @param int $id
     * @param Request $request
     * @return array
     */
    public function findById(int $id, Request $request): array
    {
        $book = $this->bookRepository->findOneById($id);

        $book = is_null($book) ? [] : $this->translationService->translate($book, $request);

        return $book;
    }

}