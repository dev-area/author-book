<?php


namespace App\Service;


use Symfony\Component\Form\FormInterface;

class FormService
{

    /**
     * @return string[]
     */
    public function getUnsubmitMessage(): array
    {
        return ['message' => 'Form is not submitted'];
    }

    /**
     * @param FormInterface $form
     * @return string[]
     */
    public function getInvalidMessages(FormInterface $form): array
    {
        $messages = [];
        foreach ($form->getErrors(true, true) as $error) {
            $field = str_replace(['children[', '].data'], '', $error->getCause()->getPropertyPath());
            $messages[$field] = $error->getMessage();
        }
        return $messages;
    }
}