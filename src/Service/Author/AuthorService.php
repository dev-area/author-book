<?php


namespace App\Service\Author;


use App\Entity\Author;
use App\Repository\AuthorRepositoryInterface;

class AuthorService
{
    /**
     * @var AuthorRepositoryInterface
     */
    private AuthorRepositoryInterface $authorRepository;

    /**
     * @param AuthorRepositoryInterface $authorRepository
     */
    public function __construct(AuthorRepositoryInterface $authorRepository)
    {
        $this->authorRepository = $authorRepository;
    }

    /**
     * @param Author $author
     */
    public function saveAuthor(Author $author): void
    {
        $this->authorRepository->save($author);
    }

}