<?php

namespace App\Controller;

use App\Entity\Author;
use App\Form\AuthorFormType;
use App\Service\Author\AuthorService;
use App\Service\FormService;
use Doctrine\DBAL\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthorController extends AbstractApiController
{
    /**
     * @var FormService
     */
    private FormService $formService;
    /**
     * @var AuthorService
     */
    private AuthorService $authorService;

    public function __construct(FormService $formService, AuthorService $authorService)
    {
        $this->formService = $formService;
        $this->authorService = $authorService;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request): Response
    {
        $form = $this->buildForm(AuthorFormType::class)->handleRequest($request);

        if (!$form->isSubmitted()) {
            $message = $this->formService->getUnsubmitMessage();
            return $this->respond($message);
        }

        if (!$form->isValid()) {
            $messages = $this->formService->getInvalidMessages($form);
            return $this->respond($messages);
        }

        /** @var Author $author */
        $author = $form->getData();

        try {
            $this->authorService->saveAuthor($author);
        } catch (Exception $exception) {
            // TODO implement catch
        }

        return $this->respond($author);
    }
}
