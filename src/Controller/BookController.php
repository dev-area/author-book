<?php


namespace App\Controller;


use App\Entity\Book;
use App\Form\BookFormType;
use App\Service\Book\BookService;
use App\Service\FormService;
use Doctrine\DBAL\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BookController extends AbstractApiController
{
    /**
     * @var FormService
     */
    private FormService $formService;
    /**
     * @var BookService
     */
    private BookService $bookService;

    public function __construct(FormService $formService,
                                BookService $bookService)
    {
        $this->formService = $formService;
        $this->bookService = $bookService;
    }

    public function createAction(Request $request): Response
    {
        $form = $this->buildForm(BookFormType::class)->handleRequest($request);

        if (!$form->isSubmitted()) {
            $message = $this->formService->getUnsubmitMessage();
            return $this->respond($message);
        }

        if (!$form->isValid()) {
            $messages = $this->formService->getInvalidMessages($form);
            return $this->respond($messages);
        }

        /** @var Book $book */
        $book = $form->getData();

        try {
            $this->bookService->saveBook($book);
        } catch (Exception $exception) {
            // TODO implement catch
        }

        return $this->respond($book);
    }


    /**
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request): Response
    {
        $target = $request->get('target');

        $books = $this->bookService->searchBook($target);

        return $this->respond($books);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showAction(Request $request): Response
    {
        $bookId = $request->get('id');

        $book = $this->bookService->findById($bookId, $request);

        return $this->respond($book);
    }

}