<?php

namespace App\Repository;

use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository implements BookRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct($registry, Book::class);
    }

    /**
     * @param Book $book
     */
    public function save(Book $book): void
    {
        $this->entityManager->persist($book);
        $this->entityManager->flush();
    }

    /**
     * @param string $value
     * @return array
     */
    public function findAllByPartName(string $value): array
    {
        return $this->entityManager->getRepository(Book::class)->createQueryBuilder('b')
            ->where('LOWER(b.name) LIKE LOWER(:val)')
            ->setParameter('val', '%' . $value . '%')
            ->getQuery()
            ->execute()
            ;
    }

    /**
     * @param int $value
     * @return Book|null
     */
    public function findOneById(int $value): ?Book
    {
        return $this->entityManager->getRepository(Book::class)->createQueryBuilder('b')
            ->where('b.id = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
}
