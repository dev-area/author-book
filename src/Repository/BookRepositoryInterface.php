<?php


namespace App\Repository;


use App\Entity\Book;

interface BookRepositoryInterface
{
    /**
     * @param Book $book
     */
    public function save(Book $book): void;

    /**
     * @param string $value
     * @return array
     */
    public function findAllByPartName(string $value): array;

    /**
     * @param int $value
     * @return Book|null
     */
    public function findOneById(int $value): ?Book;

}