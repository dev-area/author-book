<?php


namespace App\Repository;


use App\Entity\Author;

interface AuthorRepositoryInterface
{

    /**
     * @param Author $author
     * @return void
     */
    public function save(Author $author): void;

}