<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Book;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $authors = ['А.С. Пушкин', 'М.Ю. Лермонтов', 'Л.Н. Толстой', 'А.П. Чехов'
            , 'Ф.М. Достоевский', 'Н.В. Гоголь', 'М.А. Булгаков'];

        $books = ['Eugene Onegin|Евгений Онегин'
            , 'A Hero of Our Time|Герой нашего времени'
            , 'War And Peace|Война и мир'
            , 'The Cherry Orchard|Вишневый Сад'
            , 'Crime and Punishment|Преступление и наказание'
            , 'Dead souls|Мёртвые души'
            , 'The Master and Margarita|Мастер и Маргарита'
        ];


        $part = 50;
        for ($i = 1; $i <= 10000; $i++) {
            $r = rand(0, 6);
            $author = new Author();
            $author->setName($authors[$r] . ' ' . $i);

            $book = new Book();
            $book->setName($books[$r] . '|' . $i);
            $book->addAuthor($author);

            $manager->persist($author);
            $manager->persist($book);
            if ($i % $part === 0) {
                $manager->flush();
                $manager->clear();
            }
        }


    }

    public function getOrder(): int
    {
        return 0;
    }
}
